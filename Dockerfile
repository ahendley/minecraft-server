# Minecraft Dockerfile - Example with notes

# Use the offical Debian Docker image with a specified version tag, Wheezy, so not all
# versions of Debian images are downloaded.
FROM ubuntu:latest

# ARG minecraft_version
ARG minecraft_download_url

# Use APT (Advanced Packaging Tool) built in the Linux distro to download Java, a dependency
# to run Minecraft. RUN apt-get -y update
RUN apt-get update && \
	apt-get upgrade -q -y && \
	apt-get install -q -y --no-install-recommends wget && \
	apt-get install -q -y --no-install-recommends openjdk-17-jre-headless && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*

# Download Minecraft Server components
# RUN wget -q https://s3.amazonaws.com/Minecraft.Download/versions/${minecraft_version}/minecraft_server.${minecraft_version}.jar
RUN wget "${minecraft_download_url}"

# Sets working directory for the CMD instruction (also works for RUN, ENTRYPOINT commands)
# Create mount point, and mark it as holding externally mounted volume
WORKDIR /data
VOLUME /data

# Expose the container's network port: 25565 during runtime.
EXPOSE 25565

#Automatically accept Minecraft EULA, and start Minecraft server
CMD echo eula=true > /data/eula.txt && java -Xmx1024M -Xms1024M -jar /server.jar nogui
