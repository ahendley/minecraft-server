# Minecraft Data Dockerfile - Example with notes

# Use the offical Debian Docker image with a specified version tag, Wheezy, so not all
# versions of Debian images are downloaded.
FROM ubuntu:latest

MAINTAINER Andrew Hendley <amhendley@gmail.com>

# Use APT (Advanced Packaging Tool) built in the Linux distro to download Java, a dependency
# to run Minecraft. RUN apt-get -y update
RUN apt-get update && \
	apt-get upgrade -q -y && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*

# Sets working directory for the CMD instruction (also works for RUN, ENTRYPOINT commands)
# Create mount point, and mark it as holding externally mounted volume
WORKDIR /data
VOLUME /data
