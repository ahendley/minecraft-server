/* groovylint-disable CompileStatic, CouldBeSwitchStatement, DuplicateStringLiteral, NestedBlockDepth, NoTabCharacter */
pipeline {
    agent any

    parameters {
        booleanParam(name: 'FORCE_DEPLOYMENT', defaultValue: false)
    }
    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        stage('Initialise') {
            steps {
                script {
                    String version = ''
                    String tag = ''
                    Integer imageCount = 0

                    sh '''rm -f version_manifest.json
                    curl https://launchermeta.mojang.com/mc/game/version_manifest.json -o version_manifest.json
                    '''

                    echo 'INFO: Setting build date'
                    now = new Date()
                    build_date = now.format('yyyyMMdd_HHmmss', TimeZone.getTimeZone('UTC'))
                    env['BUILD_DATE'] = build_date

                    echo 'INFO: Reading version manifest'
                    manifest = readJSON file: 'version_manifest.json'
                    version = manifest.latest.release

                    for (ver in manifest.versions) {
                        if (ver.id == version) {
                            version_url = ver.url
                            break
                        }
                    }

                    echo "INFO: version url is '${version_url}'"

                    version_parts = version_url.split('/')
                    version_filename = version_parts[version_parts.size() - 1]

                    echo "INFO: version filename is '${version_filename}'"

                    sh """rm -f ${version_filename}
                    curl "${version_url}" -o "${version_filename}"
                    """

                    version_manifest = readJSON file: "${version_filename}"
                    download_url = version_manifest.downloads.server.url
                    env['DOWNLOAD_URL'] = download_url

                    echo "INFO: Discovered version from Mojang is - ${version}"
                    echo "INFO: Download URL is - ${download_url}"

                    tag = "minecraft-server:$version"
                    env['IMAGE_TAG'] = tag

                    imageCount = sh(
                        script: "docker images ${tag} | wc -l",
                        returnStdout: true
                    ).trim().toInteger() - 1
                    env['IMAGE_COUNT'] = imageCount

                    cid = sh(script: 'docker ps -f name=minecraft -q', returnStdout: true).trim()
                    env['DOCKER_CID'] = cid
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    if ("${IMAGE_COUNT}" == '0') {
                        echo 'INFO: Performing Docker build...'
                        sh "docker build --tag='${IMAGE_TAG}' --build-arg minecraft_download_url=${DOWNLOAD_URL} --file minecraft.Dockerfile ."
                        // sh "docker build --tag='${tag}' --build-arg minecraft_version=${version} --file minecraft.Dockerfile ."

                        /*
                        dockerImage = docker.build("simple-deployment:$DEPLOYMENT_VERSION.$BUILD_NUMBER")
                        */
                    } else {
                        echo "WARN: Docker image '${IMAGE_TAG}' already exists."
                    }
                }
            }
        }
        stage('Test') {
            steps {
                script {
                    if ("${IMAGE_COUNT}" == '0') {
                        echo 'INFO: Test docker image here...'
                        /*
                        dockerImage.inside {
                            sh 'echo "Tests passed"'
                        }
                        */
                    } else {
                        echo "INFO: Docker image '${IMAGE_TAG}' already exists, so no testing."
                    }
                }
            }
        }
        stage('Backup') {
            steps {
                script {
                    if ((("${IMAGE_COUNT}" == '0') && ("${DOCKER_CID}" != '')) || ("${FORCE_DEPLOYMENT}" == 'true')) {
                        echo 'INFO: Stop existing running docker container'
                        sh """
                        docker stop minecraft

                        mkdir -p /data/backups/minecraft
                        cd /data/minecraft
                        tar -czvf /data/backups/minecraft/minecraft-${BUILD_DATE}.tar.gz *
                        """
                    } else {
                        echo "INFO: Docker image '${IMAGE_TAG}' already exists. Backup not performed."
                    }
                }
            }
        }
        stage('Deploy') {
            steps {
                script {
                    echoVariables()

                    if ((("${IMAGE_COUNT}" == '0') && ("${DOCKER_CID}" != '')) || ("${FORCE_DEPLOYMENT}" == 'true')) {
                        echo 'INFO: Removing existing docker container'
                        sh 'docker rm minecraft'

                        echo 'INFO: Recreate docker container'
                        sh "docker run -d --name=minecraft --restart=always -p 25565:25565 -v /data/minecraft:/data ${IMAGE_TAG}"
                    } else {
                        echo "INFO: Docker image '${IMAGE_TAG}' already exists. No need need to deploy."
                    }
                }
            }
        }
    }
}

def echoVariables() {
    echo """
    Image Tag: ${IMAGE_TAG}
    Image Count: ${IMAGE_COUNT}
    Docker CID: ${DOCKER_CID}
    Download Url: ${DOWNLOAD_URL}
    Force Deployment: ${FORCE_DEPLOYMENT}
    Build Date: ${BUILD_DATE}
    """
}
